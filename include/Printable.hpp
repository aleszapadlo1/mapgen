#ifndef PRINTABLE_H
#define PRINTABLE_H

#include <inttypes.h>
#include <xtensor.hpp>
#include <concepts>

#include "ppm_io.h"

#include <Common.hpp>
#include <Point.hpp>

struct Coords{
    coordinates_type row;
    coordinates_type col;
};

template <typename T> class Printable {
public:
    xt::xarray<T, xt::layout_type::row_major> * data_tensor;
    coordinates_type map_dimension;
    std::vector<size_t> shape;
    bool heightmap = false;

    Printable(coordinates_type _dimensions);
    ~Printable(){
        delete data_tensor;
    }

    void generatePPMimage(const std::string _filename);

    Coords getCoordsFromVertIndex(const coordinates_type _vertIndex);

    //for simple numeric elevation maps - greyscale
    void setMapColorPoint(uint8_t * _buffer, uint_fast64_t &position, terrain_type::iterator _elevation) ;

    //for more complicated "Point type" maps
    void setMapColorPoint(uint8_t * _buffer, uint_fast64_t &_position, map_type::iterator _point);

};

template <typename T> Printable<T>::Printable(coordinates_type _dimensions){
    map_dimension = _dimensions;
    //X-X square array
    shape = {_dimensions, _dimensions};
    data_tensor = new xt::xarray<T, xt::layout_type::row_major> (xt::xarray<T, xt::layout_type::row_major>::from_shape(shape));
}

//for simple numeric elevation maps - greyscale
template <typename T> void Printable<T>::setMapColorPoint(uint8_t * _buffer, uint_fast64_t &_position, terrain_type::iterator _elevation) {

        const uint8_t channel = std::floor((*_elevation) * 255);

        //R
        _buffer[_position++] = channel; //adds the value, than increments position counter
        //G
        _buffer[_position++] = channel;
        //B
        _buffer[_position++] = channel;
}

//for more complicated "Point type" maps
template <typename T> void Printable<T>::setMapColorPoint(uint8_t * _buffer, uint_fast64_t &_position, map_type::iterator _point){

        //main mapping algorithm transforming elevation to color
        _point->transformElevationToRGB(heightmap);
        _buffer[_position++] = _point->getColor(RGB::red); //adds the value, than increments position counter
        _buffer[_position++] = _point->getColor(RGB::green);
        _buffer[_position++] = _point->getColor(RGB::blue);
}

template <typename T>
void Printable<T>::generatePPMimage(const std::string _filename){

    uint_fast64_t image_length = map_dimension * map_dimension * 3; // 3 refers to the number of RGB channels

    //or else the large array will be located on stack, causing stackoverflow
    uint8_t * buffer = new uint8_t[image_length];

    uint_fast64_t position = 0;

    //fill the buffer with R,G,B triples corresponding to the points
    for (auto it = (*data_tensor).begin(); it!= (*data_tensor).end(); ++it){
        if(position + 3 > image_length){
            throw std::runtime_error("Exceeded buffer size");
        }

        //differs based on type of points in the tensor
        setMapColorPoint(buffer, position, it);
    }
    
    int status;
    const std::string magic ("P3");
    PPM ppmOut(buffer,map_dimension,map_dimension,255,magic);
    std::string outFilepath = "./"+_filename+".ppm";
    // Switch between P3 and P6
    ppmOut.setBinary(false);
    status = ppmOut.write(outFilepath);

    //free the memory
    delete[] buffer;
}

//returns [0, map_dimension-1]
template <typename T>
Coords Printable<T>::getCoordsFromVertIndex(const coordinates_type _vertIndex){
    Coords c {
            .row = (_vertIndex == 0) ? 0 : static_cast<coordinates_type>(floor(_vertIndex/map_dimension)),
            .col = static_cast<coordinates_type>(_vertIndex%map_dimension)
    };
    return c;
}


#endif