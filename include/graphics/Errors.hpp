//
// Created by 42R0N on 14.08.2023.
//

#ifndef MAPGEN_ERRORS_HPP
#define MAPGEN_ERRORS_HPP

#include <Common.hpp>

//returns human readable error string from the error number; stitched together in a moment, not the best code
std::string getErrorName(const GLenum _errorCode){
    std::string error;

    switch (_errorCode)
    {
        case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
        case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
        case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
            //case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
            //case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
        case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
        case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
    }

    return error;
}

GLenum glCheckError_(const char *file, int line)
{

    GLenum ret = glGetError();

    if (ret != GL_NO_ERROR)
    {
        GLuint finite = 255; // (watchdog count)
        GLenum gl_err = ret;

        while (gl_err != GL_NO_ERROR && finite--){

            std::cout << getErrorName(gl_err) << " | " << file << " (" << line << ")" << std::endl;

            //glGetError needs to be called inside the loop to clear it
            gl_err = glGetError();
        }

        if (gl_err != GL_NO_ERROR)
            throw std::invalid_argument("OpenGL error failed to clear:"+ getErrorName(gl_err));
    }

    return ret;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

#endif //MAPGEN_ERRORS_HPP
