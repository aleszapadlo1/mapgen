#ifndef EBO_H
#define EBO_H

#include<glad/glad.h>


class EBO
{
public:

    GLuint ID;

    EBO(GLuint* _indicies, GLsizeiptr _size);
    ~EBO() = default;

    void bind();
    void unbind();
    void terminate();
};

EBO::EBO(GLuint* _indicies, GLsizeiptr _size){
	glGenBuffers(1, &ID);
	// Make the EBO the current Vertex Array Object by binding it
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ID);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _size, _indicies, GL_STATIC_DRAW);

}

void EBO::bind(){
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID);
}

void EBO::unbind(){
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void EBO::terminate(){
	glDeleteBuffers(1, &ID);
}



#endif
