#ifndef VAO_H
#define VAO_H

#include<glad/glad.h>
#include<VBO.hpp>


class VAO
{
public:

    GLuint ID;

    VAO();
    ~VAO() = default;

    void linkAttribute(VBO& _VBO, GLuint _layout, GLuint _componentNum, GLenum _type, GLboolean _normalized, GLuint _stride, void* _offset);
    void linkVBO(VBO& _VBO, GLuint _layout);
    void bind();
    void unbind();
    void terminate();
};

VAO::VAO(){
	glGenVertexArrays(1, &ID);

}

void VAO::linkAttribute(VBO& _VBO, GLuint _layout, GLuint _componentNum, GLenum _type, GLboolean _normalized, GLuint _stride, void* _offset){
    _VBO.bind();

    // Configure the Vertex Attribute so that OpenGL knows how to read the VBO
    glVertexAttribPointer(_layout, _componentNum, _type, _normalized, _stride, _offset);
    // Enable the Vertex Attribute so that OpenGL knows to use it
    glEnableVertexAttribArray(_layout);

    _VBO.unbind();
}


void VAO::linkVBO(VBO& _VBO, GLuint _layout){
    _VBO.bind();

    // Configure the Vertex Attribute so that OpenGL knows how to read the VBO
	glVertexAttribPointer(_layout, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);
	// Enable the Vertex Attribute so that OpenGL knows to use it
	glEnableVertexAttribArray(_layout);

    _VBO.unbind();

}

void VAO::bind(){
	glBindVertexArray(ID);
}

void VAO::unbind(){
	glBindVertexArray(0);
}

void VAO::terminate(){
	glDeleteVertexArrays(1, &ID);
}



#endif
