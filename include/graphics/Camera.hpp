//
// Created by 42R0N on 14.08.2023.
//

#ifndef MAPGEN_CAMERA_HPP
#define MAPGEN_CAMERA_HPP

#include <glad/glad.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <Errors.hpp>
#include <Shader.hpp>

class Camera{
public:

    Camera(unsigned int _width, unsigned int _height, glm::vec3 _position, glm::vec3 _orientation);
    ~Camera() = default;

    // Initializes matrices so they are not the null matrix
    //glm::mat4 model = glm::mat4(1.0f);
    glm::mat4 view = glm::mat4(1.0f);
    glm::mat4 proj = glm::mat4(1.0f);

    glm::vec3 position;
    glm::vec3 orientation;

    glm::vec3 up = glm::vec3(0.0f,1.0f,0.0f);

    const float speed = 0.05f;
    float speedBoost = 0.0f;

    bool firstClick = true;
    double cursorX = 0;
    double cursorY = 0;
    const float sensitivity = 100.0f;

    unsigned int width, height;

    void updateCamera(float _FOVdeg,float _nearPlane, float _farPlane);
    void sendCameraToShader(Shader& _shader, const char * _cameraTransformationUniform);

    float getCameraSpeed();
    void inputs(GLFWwindow* _window);
    void recenterCursor(GLFWwindow * _window);
    void rotateCamera(const float _rotXDeg,const float _rotYDeg);
};

Camera::Camera(unsigned int _width, unsigned int _height, glm::vec3 _position, glm::vec3 _orientation):width(_width),height(_height),position(_position),orientation(_orientation) {}

void Camera::updateCamera(float _FOVdeg, float _nearPlane, float _farPlane) {

    // Assigns different transformations to each matrix
    // model = glm::rotate(model, glm::radians(rotation), up);
    view = glm::lookAt(position,position+orientation,up);
    proj = glm::perspective(glm::radians(_FOVdeg), (float)width / (float)height, _nearPlane, _farPlane);
}

void Camera::sendCameraToShader(Shader &_shader, const char *_cameraTransformationUniform) {

    // Outputs the matrices into the Vertex Shader
    int cameraTransformation = glGetUniformLocation(_shader.ID, _cameraTransformationUniform);

    //if the glGetUniformLocation returns -1, the uniform is not used in the shader
    if(cameraTransformation == -1){
        std::cout << "Variable '" << _cameraTransformationUniform <<"' could not be found in the shader; shader might have failed the compilation" << std::endl;
    }

    glUniformMatrix4fv(cameraTransformation, 1, GL_FALSE, glm::value_ptr(proj * view));
    glCheckError();
}


float Camera::getCameraSpeed(){
    return speed + speedBoost;
}

void Camera::recenterCursor(GLFWwindow * _window) {
    cursorX = height/2;
    cursorY = width/2;
    glfwSetCursorPos(_window,cursorX,cursorY);
}


void Camera::inputs(GLFWwindow *_window) {
    if(glfwGetKey(_window,GLFW_KEY_W) == GLFW_PRESS){
        position += getCameraSpeed() * orientation;
    }

    if(glfwGetKey(_window,GLFW_KEY_S) == GLFW_PRESS){
        position += getCameraSpeed() * -orientation;
    }

    if(glfwGetKey(_window,GLFW_KEY_A) == GLFW_PRESS){
        position += getCameraSpeed() * -glm::normalize(glm::cross(orientation,up));
    }

    if(glfwGetKey(_window,GLFW_KEY_D) == GLFW_PRESS){
        position += getCameraSpeed() * glm::normalize(glm::cross(orientation,up));
    }

    if(glfwGetKey(_window,GLFW_KEY_SPACE) == GLFW_PRESS){
        position += getCameraSpeed() * up;
    }

    if(glfwGetKey(_window,GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS){
        position += getCameraSpeed() * -up;
    }

    if(glfwGetKey(_window,GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS){
        speedBoost = speed; //double the speed of the camera
    }

    if(glfwGetKey(_window,GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE){
        speedBoost = 0; //slow back down
    }

    if(glfwGetMouseButton(_window,GLFW_MOUSE_BUTTON_LEFT ) == GLFW_PRESS){

        if(firstClick){
            recenterCursor(_window);
            firstClick = false;
        }
        //hide the cursor
        glfwSetInputMode(_window,GLFW_CURSOR,GLFW_CURSOR_HIDDEN);

        //get cursor location
        glfwGetCursorPos(_window,&cursorX,&cursorY);

        float rotX = sensitivity * static_cast<float>( cursorY - (height/2))/height;
        float rotY = sensitivity * static_cast<float>( cursorX - (height/2))/height;

        rotateCamera(rotX,rotY);

        recenterCursor(_window);

    }else if(glfwGetMouseButton(_window,GLFW_MOUSE_BUTTON_LEFT ) == GLFW_RELEASE){
        glfwSetInputMode(_window,GLFW_CURSOR,GLFW_CURSOR_NORMAL);
        firstClick = true;
    }

}

void Camera::rotateCamera(const float _rotXDeg, const float _rotYDeg) {

    //vertical rotation
    glm::vec3 newOrientation = glm::rotate(orientation,glm::radians(-_rotXDeg),glm::normalize(glm::cross(orientation,up)));

    const float angleLock = glm::radians(5.0f);
    const float angleToUp = glm::angle(newOrientation,up);
    const float angleToBottom = glm::angle(newOrientation,-up);
    //prevent gimbal lock when looking up
    if((angleToUp >= angleLock && angleToBottom >= angleLock)){
        orientation = newOrientation;
    }

    //horizontal rotation
    //always apply, no gimbal lock exists here
    orientation = glm::rotate(orientation,glm::radians(-_rotYDeg),up);
}



#endif //MAPGEN_CAMERA_HPP
