#ifndef VBO_H
#define VBO_H

#include<glad/glad.h>


class VBO
{
public:

    GLuint ID;

    VBO(GLfloat* _vertices, GLsizeiptr _size);
    ~VBO() = default;

    void bind();
    void unbind();
    void terminate();
};

VBO::VBO(GLfloat* _vertices, GLsizeiptr _size){
	glGenBuffers(1, &ID);
	// Bind the VBO specifying it's a GL_ARRAY_BUFFER
	glBindBuffer(GL_ARRAY_BUFFER, ID);
	// Introduce the vertices into the VBO
	glBufferData(GL_ARRAY_BUFFER, _size, _vertices, GL_STATIC_DRAW);

}

void VBO::bind(){
	glBindBuffer(GL_ARRAY_BUFFER, ID);
}

void VBO::unbind(){
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VBO::terminate(){
	glDeleteBuffers(1, &ID);
}





#endif
