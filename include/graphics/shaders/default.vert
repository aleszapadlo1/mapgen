#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNormal;

// Outputs the color for the Fragment Shader
out vec3 color;
// Outputs the texture coordinates to the Fragment Shader
out vec2 texCoord;
// Outputs the normal for the Fragment Shader
out vec3 normal;
// Outputs the current position for the Fragment Shader
out vec3 currentPos;

// Imports the camera matrix from the main function
uniform mat4 cameraTransformation;
// Imports the model matrix from the main function
uniform mat4 model;

void main()
{
   // calculates current position
   currentPos = vec3(model * vec4(aPos, 1.0f));
   gl_Position = cameraTransformation * vec4(currentPos, 1.0);
   color = aColor;
   normal = aNormal;

}