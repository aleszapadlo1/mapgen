#version 330 core

//outputs color to be displayed
out vec4 FragColor;

// Imports the color from the Vertex Shader
in vec3 color;
// Imports the normal from the Vertex Shader
in vec3 normal;
// Imports the current position from the Vertex Shader
in vec3 currentPos;

// Gets the Texture Unit from the main function
uniform sampler2D tex0;
// Gets the color of the light from the main function
uniform vec4 lightColor;
// Gets the position of the light from the main function
uniform vec3 lightPos;
// Gets the position of the camera from the main function
uniform vec3 camPos;

void main()
{
   //FragColor = texture(tex0,texCoord);
   //FragColor = vec4(color,1.0f);

   float ambient = 0.20f;

   // diffuse lighting
   vec3 normalVec = normalize(normal);
   vec3 lightDirection = normalize(lightPos - currentPos);
   float diffuse = max(dot(normalVec, lightDirection), 0.0f);

   // specular lighting
   float specularLight = 0.50f;
   vec3 viewDirection = normalize(camPos - currentPos);
   vec3 reflectionDirection = reflect(-lightDirection, normalVec);
   float specAmount = pow(max(dot(viewDirection, reflectionDirection), 0.0f), 8);
   float specular = specAmount * specularLight;

   // outputs final color
   FragColor = vec4(color,1.0f) * lightColor * (diffuse + ambient + specular);

}