#ifndef SHADER_H
#define SHADER_H

#include<glad/glad.h>
#include<string>
#include<fstream>
#include<sstream>
#include<iostream>
#include<cerrno>


// Reads a text file and outputs a string with everything in the text file
std::string get_file_contents(const char* filename)
{
	std::ifstream in(filename, std::ios::binary);
	if (in)
	{
		std::string contents;
		in.seekg(0, std::ios::end);
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();
		return(contents);
	}
	throw(errno);
}

class Shader{
    
public:
    // Reference ID of the Shader Program
	GLuint ID;

    Shader(const char* vertexFile, const char* fragmentFile);
    ~Shader() = default;

	// Activates the Shader Program
	void activate();
	// Deletes the Shader Program
	void terminate();
};

//gets the source code for the shaders and compiles them
GLuint compileShader(const GLuint _type, std::string& _source){
	const char* rawString = _source.c_str();

	// Create Vertex Shader Object and get its reference
	GLuint shaderID = glCreateShader(_type);
	// Attach Vertex Shader source to the Vertex Shader Object
	glShaderSource(shaderID, 1, &rawString, NULL);
	// Compile the Vertex Shader into machine code
	glCompileShader(shaderID);

	GLint result;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	if(result == GL_FALSE){
		GLint length;
		glGetShaderiv(shaderID,GL_INFO_LOG_LENGTH, &length);

		char* errorMessage = new char[length];
		glGetShaderInfoLog(shaderID, length,&length, errorMessage);

		std::cout<< "Failed to compile " << (_type == GL_VERTEX_SHADER ? "vertex " : "fragment " ) << "shader"<<std::endl << errorMessage << std::endl;
	}

	return shaderID;
}

Shader::Shader(const char* vertexFile, const char* fragmentFile){
    // Read vertexFile and fragmentFile and store the strings
	std::string vertexCode = get_file_contents(vertexFile);
	std::string fragmentCode = get_file_contents(fragmentFile);

	//compiles vcode for vertex and source shaders
	GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vertexCode);
	GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentCode);

	//TODO: error handling

	// Create Shader Program Object and get its reference
	ID = glCreateProgram();
	// Attach the Vertex and Fragment Shaders to the Shader Program
	glAttachShader(ID, vertexShader);
	glAttachShader(ID, fragmentShader);
	// Wrap-up/Link all the shaders together into the Shader Program
	glLinkProgram(ID);

	//whether the compiled shaders in program can run 
	glValidateProgram(ID);

	// Delete the now useless Vertex and Fragment Shader objects
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

// Activates the Shader Program
void Shader::activate()
{
	glUseProgram(ID);
}

// Deletes the Shader Program
void Shader::terminate()
{
	glDeleteProgram(ID);
}

#endif
