//
// Created by 42R0N on 11.08.2023.
//

#ifndef MAPGEN_TEXTURE_H
#define MAPGEN_TEXTURE_H

#include <Shader.hpp>

#include <glad/glad.h>
#include <stb/stb_image.h>

class Texture {
private:
    GLuint ID;
    GLuint slot;

    const char * filepath;
    unsigned char * imgBytes;
    int imgWidth,imgHeight,imgColorChNum;


public:
    Texture(const char * _filepath,const GLuint _slot);

     void texUnit(Shader& _shader, const char* _uniform, GLint _unit);
     void bind();
     void unbind();
     void terminate();
};

Texture::Texture(const char * _filepath,const GLuint _slot) : filepath(_filepath), imgWidth(0), imgHeight(0), imgColorChNum(0), slot(0) {

    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    stbi_set_flip_vertically_on_load(true);

    imgBytes = stbi_load(_filepath,&imgWidth,&imgHeight,&imgColorChNum,4);

    glGenTextures(1,&ID);

    this->bind();

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,imgWidth,imgHeight,0,GL_RGBA,GL_UNSIGNED_BYTE,imgBytes);
    glGenerateMipmap(GL_TEXTURE_2D);

    //it is already load in the OpenGL Texture object
    stbi_image_free(imgBytes);

    this->unbind();
}


void Texture::texUnit(Shader& _shader, const char* _uniform, GLint _unit)
{
    // Gets the location of the uniform
    GLint texUni = glGetUniformLocation(_shader.ID, _uniform);
    // Shader needs to be activated before changing the value of a uniform
    _shader.activate();
    // Sets the value of the uniform
    glUniform1i(texUni, _unit);
}

void Texture::terminate() {
    //to be sure that no texture is bind
    this->unbind();
    glDeleteTextures(1,&ID);
}

void Texture::unbind() {
    glBindTexture(GL_TEXTURE_2D,0);
}

void Texture::bind() {
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D,ID);
}


#endif //MAPGEN_TEXTURE_H
