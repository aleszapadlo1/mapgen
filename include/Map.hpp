#ifndef MAP_H
#define MAP_H

#include <inttypes.h>
#include <string>
#include <iterator>
#include <algorithm>
#include <math.h>
#include <numbers>

#include <xtensor.hpp>
#include <PerlinNoise.hpp>
#include "ppm_io.h"
//#include <spng.h>

#include <Common.hpp>
#include <Printable.hpp>
#include <Terrain.hpp>
#include <Measurement.hpp>
#include <Raiblock.hpp>

#include <glm/glm.hpp> // glm::vec3
#include <glm/gtx/normal.hpp>



class Map : public Printable<Point>
{
//Allow the Raiblock to access the terrain parameters
friend class Rainblock;

private:
    //X-X row_major array
    //map_type data_tensor;
    //coordinates_type map_dimension;
public:
    using Printable::Printable; //inhereted constructor
    ~Map(){
        if(vertexBuffer != nullptr){
            delete vertexBuffer;
        }

        if(indexBuffer != nullptr){
            delete indexBuffer;
        }
    }
    unsigned int mapHorizontalSize = 0; //map axis length in meters
    unsigned int mapVerticalSize = 0; //height in meters

    VBO_type * vertexBuffer = nullptr;
    unsigned int vertexBufferSize = 0;

    VBO_index_type * indexBuffer = nullptr;
    unsigned int indexBufferSize = 0;

    void transformTerrainIntoMap(const Terrain & _terrain);
    void calculatePointNormals();

    VBO_type * fillVertexBuffer();
    VBO_index_type * fillIndexBuffer();
    std::vector<uint_fast64_t> vertexNeigbourVertices(const uint_fast64_t _index);

    const double disperseSedimentation(const std::vector<Point> & _neighbourPoints, const Point & _currentPoint, const Position & _nextPosition,  const double & _sedimentAmount);
    void disperseErosion(const std::vector<Point> & _neighbourLastPoints, const Point & _lastPoint, const Position & _currentPos, const double & _sedimentAmount);

    const std::vector<Point> availablePoints(const Position &_pos);
    void applyHydroErosion();
};

void Map::transformTerrainIntoMap(const Terrain & _terrain){

    mapHorizontalSize = _terrain.mapHorizontalSize;
    mapVerticalSize = _terrain.mapVerticalSize;

    const double verticalScale = 0.5;
    const uint_fast16_t modelScale = 1;

    uint_fast64_t i = 0;
    for (auto it = _terrain.data_tensor->begin(); it!= _terrain.data_tensor->end(); ++it){

        //compute row and column coordinates
        const Coords coords = getCoordsFromVertIndex(i);


        //the coordinate in the openGL local space, with the object placed in the center
        //required for the normals calculation to work correctly
        (*data_tensor)(coords.row,coords.col).localCoordinate = glm::vec3(
                static_cast<VBO_type>((static_cast<double>(coords.row)/static_cast<double>(map_dimension) - 0.5)) * modelScale,
                static_cast<VBO_type>(*it) * verticalScale,
                static_cast<VBO_type>((static_cast<double>(coords.col)/static_cast<double>(map_dimension) - 0.5)) * modelScale
                );

        (*data_tensor)(coords.row,coords.col).elevation = *it;
        (*data_tensor)(coords.row,coords.col).position = Position(coords.row,coords.col);


        //by calling this function, a float value from terrain map will be
        //transformed to any color required based on rules set in Point class
        (*data_tensor)(coords.row,coords.col).transformElevationToRGB(false);
        ++i;
    }
}

void Map::calculatePointNormals(){
    const std::string point_normals = "point normals";

    measure.start(point_normals);
    //calculate normals
    uint_fast64_t i = 0;
    const auto beginningIt = data_tensor->begin();
    for (auto it = beginningIt; it!= data_tensor->end(); ++it){
        std::vector<uint_fast64_t> neighbourIndexes = vertexNeigbourVertices(i);
        //multiply all the indexes by stride

        glm::vec3 normalVector(0.0f,0.0f,0.0f);

        const glm::vec3 currentPosition = it->localCoordinate;


        //this iterator will serve for transfering points from one iterations to another
        //must be initialized with value
        auto lastIterator = data_tensor->begin();
        //STARTING FROM SECOND
        // the first will be accessed by the k-1 index
        for(unsigned int k = 1; k < neighbourIndexes.size(); k++){

            //create new iterators and move them to the location of the required points (two triangle points)

            //to remove the need to recompute the k-1 point again, last k point is used as k-1 point
            auto iteratorSelector1 = (k != 1) ?  lastIterator : data_tensor->begin() + neighbourIndexes.at(k-1);
            auto iteratorSelector2 = data_tensor->begin() + neighbourIndexes.at(k);

            //calculate the normal of the triangle from the current position and the two calculated points
            glm::vec3 triangleNormal = glm::triangleNormal(currentPosition,iteratorSelector1->localCoordinate,iteratorSelector2->localCoordinate);

            //adding and subsequently normalizing will create an average of the normal vectors
            normalVector += triangleNormal;
            lastIterator = iteratorSelector2;
        }

        //assign the computed normal tu the point
        //normals are not normalised!!
        it->localNormal = glm::normalize(normalVector);
        ++i;

    }

    measure.stop(point_normals);
}


std::vector<uint_fast64_t> Map::vertexNeigbourVertices(const uint_fast64_t _index) {
    std::vector<uint_fast64_t> bufferIndexes;

    Coords coords = getCoordsFromVertIndex(_index);

    if (coords.row == 0) {
        //left top corner
        if (coords.col == 0) {
            std::vector<uint_fast64_t> b{_index + 1, _index + map_dimension};
            return b;
        }

        //right top corner
        if (coords.col == (map_dimension - 1)) {
            std::vector<uint_fast64_t> b{_index + map_dimension, _index + map_dimension - 1, _index - 1};
            return b;
        }

        //top row
        std::vector<uint_fast64_t> b{_index + 1, _index + map_dimension, _index - 1 + map_dimension, _index - 1};
        return b;

    }

    if (coords.row == (map_dimension - 1)) {
        //left bottom corner
        if (coords.col == 0) {
            std::vector<uint_fast64_t> b{_index - map_dimension, _index - map_dimension + 1, _index + 1};
            return b;
        }

        //right bottom corner
        if (coords.col == (map_dimension - 1)) {
            std::vector<uint_fast64_t> b{_index - map_dimension, _index - 1};
            return b;
        }

        //bottom row
        std::vector<uint_fast64_t> b{_index - map_dimension, _index + 1 - map_dimension, _index + 1, _index - 1};
        return b;
    }

    //left column
    if (coords.col == 0) {
        std::vector<uint_fast64_t> b{_index - map_dimension, _index + 1 - map_dimension, _index + 1,
                                     _index + map_dimension};
        return b;
    }

    //right column
    if (coords.col == (map_dimension - 1)) {
        //bottom row
        std::vector<uint_fast64_t> b{_index - map_dimension, _index + map_dimension, _index - 1 + map_dimension,
                                     _index - 1};
        return b;
    }

    //inner points
    std::vector<uint_fast64_t> b{_index - map_dimension, _index + 1 - map_dimension, _index + 1, _index + map_dimension,
                                 _index - 1 + map_dimension, _index - 1};
    return b;

}

//creates the VBO, and than returns pointer to it; pointer is saved in the class
VBO_type * Map::fillVertexBuffer() {
    //measure points
    const std::string meas_VBO = "Filling vertex buffer";
    const std::string meas_normals = "Calculating normals";


    measure.start(meas_VBO);
    const unsigned int numberOfAttributeParts = 9; //xyz + rgb +nx + ny + nz; stride
    const unsigned int dataSize = data_tensor->size();
    vertexBufferSize = numberOfAttributeParts * dataSize;
    vertexBuffer = new VBO_type[vertexBufferSize];

    VBO_type vertexBufferA[vertexBufferSize];


    uint_fast64_t i = 0;
    uint_fast64_t pointNumber = 0;
    for (auto it = data_tensor->begin(); it!= data_tensor->end(); ++it){

        const Coords coords = getCoordsFromVertIndex(pointNumber);


        //xyz in [-1,1]
        vertexBuffer[i++] = it->localCoordinate.x; //x
        vertexBuffer[i++] = it->localCoordinate.y; //y
        vertexBuffer[i++] = it->localCoordinate.z; //z


        //rgb in [0,1]
        vertexBuffer[i++] = static_cast<VBO_type>(it->getColor(RGB::red))/static_cast<VBO_type>(255); //r
        vertexBuffer[i++] =  static_cast<VBO_type>(it->getColor(RGB::green))/static_cast<VBO_type>(255);//g
        vertexBuffer[i++] = static_cast<VBO_type>(it->getColor(RGB::blue))/static_cast<VBO_type>(255); //b

        //nx, ny, nz
        vertexBuffer[i++] = it->localNormal.x; //nx
        vertexBuffer[i++] = it->localNormal.y; //ny
        vertexBuffer[i++] = it->localNormal.z; //nz

        ++pointNumber; // increase the count of calculated vertices

    }

    measure.stop(meas_VBO);
    return vertexBuffer;
}


VBO_index_type * Map::fillIndexBuffer(){

    //                 3+3     width                   height
    indexBufferSize = 6 * (map_dimension - 1) * (map_dimension - 1);
    indexBuffer = new VBO_index_type[vertexBufferSize];

    uint_fast64_t indexPos = 0;
    for (uint_fast64_t i = 0; i < (map_dimension*map_dimension); ++i) {
        const Coords coords = getCoordsFromVertIndex(i);

        //template: triangle 1 : a,a+1,a+width dim
        //template triangle 2: a+1,a+1+width,a+width

        //if its not the last column or row
        if(coords.row == map_dimension-1 || coords.col == map_dimension-1){
            continue;
        }
        //triangle one
        indexBuffer[indexPos++] = i;
        indexBuffer[indexPos++] = i+1;
        //                          width
        indexBuffer[indexPos++] = i+map_dimension;

        //triangle two
        indexBuffer[indexPos++] = i+1;
        indexBuffer[indexPos++] = i+1+map_dimension;
        indexBuffer[indexPos++] = i+map_dimension;
    }

    return indexBuffer;

}

const std::vector<Point> Map::availablePoints(const Position &_pos){
    std::vector<Point> pointsVector;

   //clockwise direction of points
    std::vector<Position> offsetsOrder = {
            Position(0,1),

            Position(1,1),
            Position(1,0),
            Position(1,-1),

            Position(0,-1),

            Position(-1,-1),
            Position(-1,0),
            Position(-1,1),

    };

    for (uint_fast8_t i = 0; i < offsetsOrder.size(); ++i) {
        const int_fast32_t xR = _pos.X + offsetsOrder[i].X;
        const int_fast32_t yR = _pos.Y + offsetsOrder[i].Y;

            //out of bounds
            if(xR < 0 || yR < 0){
                continue;
            }

            //out of bounds
            if(xR >= map_dimension || yR >= map_dimension){
                continue;
            }

            //point elevation
            pointsVector.push_back((*data_tensor)(xR,yR));
    }

    return pointsVector;
   /* //sort the vectors by lowest elevation
    std::sort(pointsVector.begin(), pointsVector.end(), [](const Point &a, const Point &b){return a.localCoordinate.y < b.localCoordinate.y; });

    return pointsVector;*/
}

glm::vec3 computePointNormal(const Point & _currentPoint, const std::vector<Point> & _points){
    Point lastPoint;
    glm::vec3 currentNormal;

    std::vector<glm::vec3> normals;

    for(unsigned int k = 1; k < _points.size(); k++){

        //create new iterators and move them to the location of the required points (two triangle points)

        //to remove the need to recompute the k-1 point again, last k point is used as k-1 point
        //auto iteratorSelector1 = (k != 1) ?  lastIterator :std::next(_points.begin(), k-1);
        const auto p1 = (k != 1) ?  lastPoint : _points[k-1];
        const auto p2 = _points[k];

        //calculate the normal of the triangle from the current position and the two calculated points
        const glm::vec3 triangleNormal = glm::triangleNormal(_currentPoint.localCoordinate,p1.localCoordinate,p2.localCoordinate);

        /*
        glm::vec3 edge1 = p1.localCoordinate - _currentPoint.localCoordinate;
        glm::vec3 edge2 = p2.localCoordinate - _currentPoint.localCoordinate;
        const glm::vec3 normal = glm::cross(edge1, edge2);*/

        normals.push_back(triangleNormal);

        //adding and subsequently normalizing will create an average of the normal vectors
        currentNormal += triangleNormal;
        lastPoint = p2;
    }
    return glm::normalize(currentNormal);;
}

const double Map::disperseSedimentation(const std::vector<Point> & _neighbourPoints, const Point & _currentPoint, const Position & _nextPosition,  const double & _sedimentAmount) {
    std::vector<uint_fast8_t> pointsIndexesLower;
    const uint_fast8_t pointsSize = _neighbourPoints.size();
    pointsIndexesLower.reserve(pointsSize);
    double totalElevationToFill = 0;
    for( uint_fast8_t k = 0; k < pointsSize; k++){
        const double elevationDiff = _neighbourPoints[k].localCoordinate.y - _currentPoint.localCoordinate.y ;
        if(elevationDiff < 0){
            pointsIndexesLower.push_back(k);
            totalElevationToFill += -elevationDiff;
        }
    }

    const uint_fast8_t centerPointHigherBy = 2;

    //Fill the points with only a fraction of the required sediment
    totalElevationToFill /= 10.0;
    const double sedimentToFill = (totalElevationToFill > _sedimentAmount) ? _sedimentAmount : totalElevationToFill;

    const double sedimentAvgPerPoint = sedimentToFill/static_cast<double>(pointsSize);

    auto computePerPointSediment = [&](){
        const double _pointsSize = static_cast<double>(pointsSize);
        return sedimentAvgPerPoint * (_pointsSize - static_cast<double>(centerPointHigherBy)) / (_pointsSize - 1);
    };

    const double perNormalPointSediment = computePerPointSediment();

    for( uint_fast8_t i = 0; i < pointsIndexesLower.size(); i++){
        const Point pointToFill = _neighbourPoints[pointsIndexesLower[i]];

        if(pointToFill.position == _nextPosition){
            (*data_tensor)(pointToFill.position.X,pointToFill.position.Y).localCoordinate.y += static_cast<double>(centerPointHigherBy) * sedimentAvgPerPoint;
            //(*data_tensor)(pointToFill.position.X,pointToFill.position.Y).setColor(Color(200,20,20));
            continue;
        }

        (*data_tensor)(pointToFill.position.X,pointToFill.position.Y).localCoordinate.y += perNormalPointSediment;
        (*data_tensor)(pointToFill.position.X,pointToFill.position.Y).localCoordinate.y += static_cast<double>(centerPointHigherBy) * sedimentAvgPerPoint;
        //(*data_tensor)(pointToFill.position.X,pointToFill.position.Y).setColor(Color(100,200,100));
    }
    //(*data_tensor)(_currentPoint.position.X,_currentPoint.position.Y).setColor(Color(250,250,250));


    return sedimentToFill;
}

void Map::disperseErosion(const std::vector<Point> & _neighbourLastPoints, const Point & _lastPoint, const Position & _currentPos, const double & _sedimentAmount){
    struct p {
        uint_fast8_t index;
        double altDiff;
        p(const uint_fast8_t _i, const double _a): index(_i), altDiff(_a){};
    };

    std::vector<p> pointsIndexesHigher;
    const uint_fast8_t pointsSize = _neighbourLastPoints.size();
    pointsIndexesHigher.reserve(pointsSize);
    double totalElevationToFill = 0;
    for( uint_fast8_t k = 0; k < pointsSize; k++){
        const double elevationDiff = _neighbourLastPoints[k].localCoordinate.y - _lastPoint.localCoordinate.y ;
        pointsIndexesHigher.push_back(p(k,elevationDiff));
    }

    for( uint_fast8_t j = 0; j < pointsIndexesHigher.size(); j++){
        const Point pointToErode = _neighbourLastPoints[pointsIndexesHigher[j].index];

        if(pointToErode.position == _currentPos){
            (*data_tensor)(pointToErode.position.X,pointToErode.position.Y).localCoordinate.y += _sedimentAmount;
            //(*data_tensor)(pointToFill.position.X,pointToFill.position.Y).setColor(Color(200,20,20));
            continue;
        }

        (*data_tensor)(pointToErode.position.X,pointToErode.position.Y).localCoordinate.y -= pointsIndexesHigher[j].altDiff/10.0 ;
        //(*data_tensor)(pointToErode.position.X,pointToErode.position.Y).setColor(Color(80,100,120));
    }
    //(*data_tensor)(_currentPoint.position.X,_currentPoint.position.Y).setColor(Color(250,250,250));
}

void Map::applyHydroErosion() {

    std::mt19937 mt{std::random_device{}()};
    //choose random point from the map span
    std::uniform_int_distribution<> rngCoord(0, map_dimension-1);
    //choose random of the 3 lowest points
    std::uniform_int_distribution<> rngPosition(0, 2);

    for (size_t i = 0; i < 100*map_dimension; i++){
        //rainblock has a random position offset
        Rainblock rainblock(rngCoord(mt),rngCoord(mt));
        Point startingPoint = (*data_tensor)(rainblock.position.X,rainblock.position.Y);

        Position lastPosition = Position(0,0);
        Point lastPoint = Point(0,0);
        for (uint_fast16_t j = 0; j < rainblock.ttl; j++){
            rainblock.position = rainblock.newRandomPos();
            if(!rainblock.positionValid(map_dimension)){
                break;
            }

            const Point currentPoint = (*data_tensor)(rainblock.position.X,rainblock.position.Y);

            //(*data_tensor)(rainblock.position.X,rainblock.position.Y).setColor(Color());

            const std::vector<Point> points = availablePoints(rainblock.position);
            glm::vec3 pointNormal = computePointNormal(currentPoint,points);
            rainblock.velocity = rainblock.computeNewVelocity(pointNormal);
            Position nextPosition = rainblock.computeNextPosition();

            if(pointNormal.y > 0.999 && rainblock.sediment != 0){
                //disperseSedimentation(points,currentPoint,nextPosition,rainblock);
                break;
            }

            double sedimentChange = rainblock.computeSedimentChange(pointNormal);

            //when the sediment is depositing, spread the deposit over all neighbour points
            const double lastSediment = rainblock.sediment;

            if(sedimentChange > 0){
                //will return the amount of sediment removed from the rainblock
                const double removedSediment = disperseSedimentation(points,currentPoint,nextPosition,sedimentChange);

                pointNormal = computePointNormal(currentPoint,points);
                rainblock.velocity = rainblock.computeNewVelocity(pointNormal);
                nextPosition = rainblock.computeNextPosition();

                rainblock.sediment -= removedSediment;



            }else {
                if(j){

                    const std::vector<Point> oldPoints = availablePoints(lastPosition);
                    disperseErosion(oldPoints,lastPoint,currentPoint.position,sedimentChange);
                }
                //added sediment
                rainblock.sediment += -sedimentChange;

            }

            //delete the block only if the rainblock is not gaining any more sediment
            const bool rateOfChange = rainblock.sediment < lastSediment;
            if(rainblock.sediment == 0.0 && !rateOfChange){
                break;
            }

            lastPoint = currentPoint;
            lastPosition=rainblock.position;
            rainblock.position = nextPosition;
        }

    }
}

#endif