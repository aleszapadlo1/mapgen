//
// Created by 42R0N on 16.08.2023.
//

#ifndef MAPGEN_MEASUREMENT_HPP
#define MAPGEN_MEASUREMENT_HPP


#include <chrono>
#include <iostream>
#include <iomanip>

class Measure{
private:

    std::map<std::string ,std::chrono::time_point<std::chrono::high_resolution_clock>> measures;

    bool doMeasurements = true;

public:
    Measure(const bool _doMeasurements = true) : doMeasurements(_doMeasurements){};
    void enableMeasurements();
    void disableMeasurements();
    void start(const std::string& _measurementName);
    void stop(const std::string& _string);
};

void Measure::start(const std::string& _measurementName) {
    //do nothing if no name is provided
    if(_measurementName.empty()) return;
    //if measurements are disabled, do nothing
    if(!doMeasurements) return;

    //check if the measurement does not exists
    if(measures.find(_measurementName) != measures.end()){
        throw std::invalid_argument("Measurement with name '" + _measurementName + "' already exists");

    }
    //add the "now" time to the map; the time can be retrieved later with the provided string
    measures.insert({_measurementName, std::chrono::high_resolution_clock::now()});
}

void Measure::stop(const std::string& _measurementName) {
    //do nothing if no name is provided
    if(_measurementName.empty()) return;
    //if measurements are disabled, do nothing
    if(!doMeasurements) return;

    //check if the measurement does exists
    auto value = measures.find(_measurementName);
    if( value == measures.end()){
        throw std::invalid_argument("Measurement with name '" + _measurementName + "' does not exists");

    }

    auto end = std::chrono::high_resolution_clock::now();

    //second is the time value stored when start() was called
    std::chrono::duration<double> diff = (end - value->second) * 1000;

    //clear the value from the map
    measures.erase(value);

    std::cout<< "'" << _measurementName << "' took: "<< std::setw(9) << diff.count() << " ms" "\n";
}


void Measure::enableMeasurements() {
    doMeasurements = true;
}

void Measure::disableMeasurements() {
    doMeasurements = false;
}


#endif //MAPGEN_MEASUREMENT_HPP
