#ifndef COMMON_H
#define COMMON_H
#include <inttypes.h>
#include <xtensor.hpp>
#include <concepts>

#include "ppm_io.h"
#include <glad/glad.h>

#include <Point.fwd.hpp>
#include <Measurement.hpp>

extern Measure measure;

using elevation_type = double;

//maximum size of the map is AT LEAST 65k x 65k
using coordinates_type = uint_fast32_t;

using terrain_type = xt::xarray<elevation_type, xt::layout_type::row_major>; 

using map_type = xt::xarray<Point, xt::layout_type::row_major>;

using VBO_type = GLfloat;
using VBO_index_type = GLuint;


//to be able to separate function based on if its a number or a random "point" class
template<typename A>
concept Arithmetic = std::integral<A> or std::floating_point<A>;

enum RGB {
    red,
    green,
    blue,
    alpha
};

struct Color {
    uint8_t r, g, b, a;

    //constructor allowing for a easy initialization of the struct, default values are 0
    Color(uint8_t _r = 0, uint8_t _g = 0, uint8_t _b = 0, uint8_t _a = 255): r(_r), g(_g), b(_b), a(_a) {};
};






#endif
