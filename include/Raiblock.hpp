//
// Created by 42R0N on 21.08.2023.
//

#ifndef MAPGEN_RAIBLOCK_HPP
#define MAPGEN_RAIBLOCK_HPP

#include <numbers>

#include <Common.hpp>
#include <Point.hpp>

#include <glm/glm.hpp> // glm::vec3
#include <glm/gtx/normal.hpp>

template <typename T> inline int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}
//Maybe use Point??
struct Position{
    //coordinates
    int_fast32_t X;
    int_fast32_t Y;

    Position(coordinates_type _x, coordinates_type _y) : X(_x),Y (_y){};
    bool operator == (const Position & b) const{
        return X == b.X && Y == b.Y;
    }

    Position & operator + (const Position & b) {
        X += b.X;
        Y += b.Y;
        return *this;
    }
};


struct Rainblock {
    //Statics
    const uint8_t ttl = 100;
    double sediment = 0; // the amount of "height" it carries

    //simple constants
    const double erosionRate = 0.01;
    const double depositionRate = 0.0002;

    //how "easily" will raiblock deposit sediment. Lower value -> more acquired sediment is needed to start depositing sediment
    const float depositPower = 1.2;

    //input of a function that decides whether raindrop can absorb more sediment. Higher value -> can carry more sediment
    const uint_fast16_t sedimentAmountErosionRolloff = 30;

    glm::vec3 velocity = glm::vec3(0.f,0.f,0.f);
    const float friction = 0.8;
    const double acceleration = 1;

    Position position;

    // random generator

    Rainblock(coordinates_type _x, coordinates_type _y) : position(Position(_x,_y)){
    };

    //the Rainblock serves more like a library of functions, rather than modifying values directly - allowing for manipulation of returned values before the rainblock itself if modified

    bool positionValid(const coordinates_type & _map_dimension) const;

    Position computeNextPosition() const;

    glm::vec3 computeNewVelocity(const glm::vec3 &_pointNormal) const;

    double computeSedimentChange(const glm::vec3 &_pointNormal) const;

    Position newRandomPos() const;

    ~Rainblock() = default;

};

bool Rainblock::positionValid(const coordinates_type & _map_dimension) const {
    if(position.X >= _map_dimension || position.Y >= _map_dimension){
        return false;
    }

    if(position.X < 0 || position.Y < 0){
        return false;
    }
    return true;
}

Position Rainblock::computeNextPosition() const{
    const float absX = std::abs(velocity.x);
    const float absY = std::abs(velocity.z);

    auto lowerThanLowAngle = [](auto &x, auto &y){
        const double yThreshold = x * 1.0/3.0;
        return y <= yThreshold;
    };

    auto lowerThanHighAngle = [](auto &x, auto &y){
        const double yThreshold = x * 3.0;
        return y <= yThreshold;
    };


    bool lowestAngle = lowerThanLowAngle(absX,absY);
    bool diagonal = lowerThanHighAngle(absX,absY);

    //the normal points in the X axis, either back or forth
    if(lowestAngle && diagonal){
        return Position(
                position.X + sgn(velocity.x),
                position.Y
        );
    }

    //diagonal point is going to be next
    if(!lowestAngle && diagonal){
        return Position(
                position.X + sgn(velocity.x),
                position.Y + sgn(velocity.z)
        );
    }

    //the normal points in the Y axis, either back or forth
    if(!lowestAngle){
        return Position(
                position.X,
                position.Y + sgn(velocity.z)
        );
    }

    throw std::invalid_argument("Incorrect next positionů; x:" + std::to_string(position.X)+ " y:"+ std::to_string(position.Y)+" velocity x:"+ std::to_string(velocity.x)+" velocity z:"+ std::to_string(velocity.z));

    return Position(0,0);
}

glm::vec3 Rainblock::computeNewVelocity(const glm::vec3 &_pointNormal) const {
    //compute new velocity
    const glm::vec3 newVelocity = glm::vec3(
            _pointNormal.x * acceleration,
            0.f,
            _pointNormal.z * acceleration);

    //first reduce the velocity by a friction; then add the new gained velocity
    return friction * velocity + newVelocity;
}

double Rainblock::computeSedimentChange(const glm::vec3 &_pointNormal) const{

    const double multiply = 100;
    const double deposit = std::pow(sediment, depositPower) * multiply * depositionRate * _pointNormal.y ;
    //const double deposit = depositionRate * _pointNormal.y ;

    //limits how much sediment one rainblock can have
    auto feedbackFunction = [&](const double &x){
        const double frequency = sedimentAmountErosionRolloff * std::numbers::pi * x - ( 3 * std::numbers::pi) * 0.5;
        return std::sin(frequency);
    };

    const double erosion = erosionRate * feedbackFunction(sediment) * (1 - _pointNormal.y);
    //const double erosion = 0.001 * (1 - pointNormal.y);
    return deposit - erosion;
}

Position Rainblock::newRandomPos() const{
    std::random_device rd;  // a seed source for the random number engine
    std::mt19937 gen(rd()); // mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> distrib(-1, 1);
    bool chance = (rand() % 100) < 10;
    if(chance){
        return Position(
                position.X + distrib(gen),
                position.Y + distrib(gen)
                );
    }
    return position;

}

#endif //MAPGEN_RAIBLOCK_HPP
