#ifndef TERRAIN_H
#define TERRAIN_H

#include <iostream>
#include <stdio.h>
#include <random> 
#include <numbers>

#include <xtensor.hpp>
#include <PerlinNoise.hpp>
#include "ppm_io.h"

//#include <Common.hpp>
#include <Printable.hpp>
//TODO move somewhere else

class Terrain : public Printable<elevation_type>
{

public:

    const unsigned int mapHorizontalSize; //map axis length in meters
    unsigned int mapVerticalSize; //height in meters

private:
    //generates a new map filled with noise of desired attributes
    const terrain_type getNoiseMap(const float scale = 1, const std::int32_t octaves = 4, const siv::PerlinNoise::value_type persistence = 0.5);


    //rainfall density represents the statistical number of rainblock particles for every point on map
    const double rainfallDensity;
    unsigned int hydroIterations;

    //Deprecated
    const double g = 9.81;
    
    const double noiseMapBaseScale = 0.0015;

    double scaleRatio;
    
public:
    Terrain(coordinates_type _dimensions, const double _rainfallDensity = 1, const unsigned int _mapHorizontalSize = 10000, const unsigned int _mapVerticalSize = 1000) : Printable<elevation_type>(_dimensions), mapHorizontalSize(_mapHorizontalSize), rainfallDensity(_rainfallDensity){

        //lock the height at a max 8000 meters
        mapVerticalSize = (_mapVerticalSize > 8000) ? 8000 : _mapVerticalSize;
        scaleRatio = mapHorizontalSize / _mapVerticalSize;

        hydroIterations = floor(_rainfallDensity * _dimensions * _dimensions);
    }
    
    ~Terrain() = default;

    terrain_type getTerrain(){ return *data_tensor;}

    void fillTerrainWithNoise();
    //sea level in the interval of [0,1]
    void createSeaLevel(const elevation_type _seaLevel);
    void applyHydroErosion();

    //void createNoiseFile(const terrain_type& _noise, const std::string filename);
    //void test();
};



//generates the elevation map of the terrain
void Terrain::fillTerrainWithNoise(){
    const float _temp_scale = noiseMapBaseScale*(map_dimension); //heuristic number that " looks ok"
     //Low frequency noise
    *data_tensor = getNoiseMap(0.3/_temp_scale,2,0.7);
    this->generatePPMimage("noise1");

    //High frequency noise
    terrain_type addNoise = getNoiseMap(1/_temp_scale,4,0.5);
    //*data_tensor += addNoise/3;
    this->generatePPMimage("noise2");

    //Very high frequency noise
    addNoise = getNoiseMap(4/_temp_scale,8,0.4);
    //*data_tensor += addNoise/8;
    this->generatePPMimage("noise3");

    //find the maximum value of the noise map 
    auto maximum = xt::amax(*data_tensor)();

    //normalize the data array
    *data_tensor /= maximum;
}

//values will be between [0,1]
const terrain_type Terrain::getNoiseMap(const float scale, const std::int32_t octaves, const siv::PerlinNoise::value_type persistence){

    //Mersenne Twister random engine - it requires changing seed (yes, for the pseudo-random function), which is provided by the random device function, which depends on the OS implementation

    //not great enough for data encryption, good enough for map generation
    std::mt19937 seed{ std::random_device{}()};
    const siv::PerlinNoise perlin{seed()};

    std::vector<coordinates_type> shape = { map_dimension, map_dimension};
    terrain_type map = terrain_type::from_shape(shape);

    const double d_step = scale * 0.01;
    
    //sets 
    for (coordinates_type y = 0; y < map_dimension; ++y){
        for (coordinates_type x = 0; x < map_dimension; ++x){
            //between 0 and 1; beneficial for normalizing the array
            map(x,y) = perlin.octave2D_01((x * d_step), (y * d_step), octaves, persistence);
        }
    }

   // std::copy(map.begin(), map.end(), std::ostream_iterator<double>(std::cout, ", "));

    size_t a = sizeof(map);

    return map;

}

void Terrain::createSeaLevel(const elevation_type _seaLevel) {


    //if the elevation is lower than the sea level, make it the sea level
    for (auto it = data_tensor->begin(); it!= data_tensor->end(); ++it){

        if(*it < _seaLevel){
            *it = _seaLevel;
        }
    }
}
#endif