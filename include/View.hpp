#ifndef VIEW_H
#define VIEW_H

#include <iostream>

#include <GraphicsHeaders.hpp>

//#include <Model.hpp>
#include <Map.fwd.hpp>

class View
{
public:
    View() = default;

    ~View(){
        glfwDestroyWindow(window);
        glfwTerminate();
    };

    GLFWwindow* window;
    //Model model; //a object containing all atributes fro displaying

    unsigned int SCREEN_WIDTH = 1200, SCREEN_HEIGHT = 800;
    const unsigned int SHADOW_WIDTH = 1200, SHADOW_HEIGHT = 1200;
    bool fullscreen = false;
    bool vsync = false;

    //Viewposition
    glm::vec3 viewPos = glm::vec3(50, 1, 50);
    bool autorotate = false;
    float rate = 0.1;
    float zoom = 0.2;
    float zoomInc = 0.001;

    //Rotation and View
    float rotation = 0.0f;
    glm::vec3 cameraPos = glm::vec3(50, 50, 50);
    glm::vec3 lookPos = glm::vec3(0, 0, 0);
    glm::mat4 camera = glm::lookAt(cameraPos, lookPos, glm::vec3(0,1,0));
    glm::mat4 projection = glm::ortho(-(float)SCREEN_WIDTH*zoom, (float)SCREEN_WIDTH*zoom, -(float)SCREEN_HEIGHT*zoom, (float)SCREEN_HEIGHT*zoom, -800.0f, 500.0f);

    bool updated = false;                 //Flag for remeshing


    //Shader Stuff
    float steepness = 0.8;
    glm::vec3 flatColor = glm::vec3(0.27, 0.64, 0.27);
    glm::vec3 steepColor = glm::vec3(0.7);

    //Lighting and Shading
    glm::vec3 skyCol = glm::vec3(0.3, 0.3f, 1.0f);
    glm::vec3 lightPos = glm::vec3(-100.0f, 100.0f, -150.0f);
    glm::vec3 lightCol = glm::vec3(1.0f, 1.0f, 0.9f);
    float lightStrength = 1.4;
    glm::mat4 depthModelMatrix = glm::mat4(1.0);
    glm::mat4 depthProjection = glm::ortho<float>(-300, 300, -300, 300, 0, 800);
    glm::mat4 depthCamera = glm::lookAt(lightPos, glm::vec3(0), glm::vec3(0,1,0));

    glm::mat4 biasMatrix = glm::mat4(
        0.5, 0.0, 0.0, 0.0,
        0.0, 0.5, 0.0, 0.0,
        0.0, 0.0, 0.5, 0.0,
        0.5, 0.5, 0.5, 1.0
    );

    //Frametime Calculation
    template<typename D>
    void calcFrameTime();
    int frameTime = 0;
    std::chrono::high_resolution_clock::time_point _old;

    bool Init();
    const bool shouldClose();
    void render();
    void inputHandler();
    void mesh(Map &map);
    void setup(Map &map);
    void cleanup();

};

void View::setup(Map &map){
    //model.setup();
   // model.reset();

    //Translate the thing inti proper space
    glm::vec3 axis = glm::vec3(-0.5*map.map_dimension, -0.5*map.mapVerticalSize, -0.5*map.map_dimension);
    //model.translate(axis);
}


bool View::Init(){

    // Initialize GLFW
    if (!glfwInit()) {
        std::cerr << "Failed to initialize GLFW" << std::endl;
        return false;
    }

    // Set OpenGL version and profile to use
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create a window
    window = glfwCreateWindow(800, 600, "OpenGL Window", nullptr, nullptr);
    if (!window) {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return false;
    }
    // Make the OpenGL context current
    glfwMakeContextCurrent(window);
    gladLoadGL();
    // Set the viewport size
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

/* 
    //window coordinates are in [-1,1] for X and [-1,1] for Y
    GLfloat vertices[] = {
        -0.5f, -0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        0.5f, 0.5f, 0.0f
    };

    // Create and compile vertex shader
    // = how the vertices should look 
    //for every vertex
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    // Create and compile fragment shader
    // = how pixels between vertices look
    //for every pixel
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);


    //adds shaders to program
    glAttachShader(shaderProgram,vertexShader);
    glAttachShader(shaderProgram,fragmentShader);

    //links the program specified earlier
    glLinkProgram(shaderProgram);

    //the shaders have already been loaded to the program 
    //and can be deleted now
    glDeleteShader(shaderProgram);
    glDeleteShader(fragmentShader);

    
    //must be before VBO!
    glGenVertexArrays(1, &VAO);
    //create an 1 ogl object to store data 
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    //bind the VBO as the current buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    //puts the vertices data into the buffer; Static that the vertices will be defined once and used many times
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    //specifies the index of the attribute, size(triangle = 3), type, whether should be normalized, stride and offset
    //this will effect the current bound buffer
    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,size_t(GL_FLOAT)*3,(void*)0);
    //enable it
    glEnableVertexAttribArray(0);

    //bind all the function to 0 to prevent any subsequent changes
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);


    
    // Initialize GLEW
    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to initialize GLEW" << std::endl;
        glfwTerminate();
        return false;
    } */

    //set window color to blue
    glClearColor(0.1f, 0.1f,0.8f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    //push the back buffer to the front, to be displayed
    glfwSwapBuffers(window);

    // Register the framebuffer size callback function
    //glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    return true;
}

void View::mesh(Map &map){
  //if(!map.updated) return;

  //Remesh the whole thing!!
  //model.fromMap(map);
  //model.update();
}

void View::inputHandler(){
    // Input handling
    glfwPollEvents();
  
}

void View::render(){
/* 
    //set window color to blue
    glClearColor(0.1f, 0.1f,0.8f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(shaderProgram);
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES,0,3);
    glfwSwapBuffers(window); */




    /* 
    // Rendering commands
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // this will push the back buffer to the front, showing it in th window
    //backbuffer can be filled again
    glfwSwapBuffers(window);



    if(autorotate){ //Auto Rotation
        glm::vec3 axis(0.0f, 1.0f, 0.0f);
        rotation += rate;
        camera = glm::rotate(camera, glm::radians(rate), axis);
    }

    */

    //glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //model.render();             //Render Scene

  /* 


  // SHADOW MAPPING 
  glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
  glBindFramebuffer(GL_FRAMEBUFFER, shadow.fbo);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  depthShader.useProgram();
  depthShader.setMat4("dmvp", depthProjection * depthCamera * model.model);
  model.render();             //Render Scene

  glBindVertexArray(0);

  //Regular Drawing

  glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
  glBindFramebuffer(GL_FRAMEBUFFER, image.fbo);
  glClearColor(skyCol.x, skyCol.y, skyCol.z, 1.0f); //Blue
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  //Use the Shader
  defaultShader.useProgram();

  //Texture Stuff
  glActiveTexture(GL_TEXTURE0+0);
  glBindTexture(GL_TEXTURE_2D, shadow.depthTexture);
  defaultShader.setInt("shadowMap", 0);
  defaultShader.setVec3("lightCol", lightCol);
  defaultShader.setVec3("lightPos", lightPos);
  defaultShader.setVec3("lookDir", lookPos-cameraPos);
  defaultShader.setFloat("lightStrength", lightStrength);
  defaultShader.setMat4("projectionCamera", projection * camera);
  defaultShader.setMat4("dbmvp", biasMatrix * depthProjection * depthCamera * glm::mat4(1.0f));
  defaultShader.setMat4("model", model.model);

  //Add uniforms for flat and steep color!
  defaultShader.setVec3("flatColor", flatColor);
  defaultShader.setVec3("steepColor", steepColor);
  defaultShader.setFloat("steepness", steepness);

  model.render();             //Render Scene

  //Render to screen using the effect shader!
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  effectShader.useProgram();
  */

  /*
  blurShader.setFloat("mousex", focus.x);
  blurShader.setFloat("mousey", focus.y);
  blurShader.setFloat("width", SCREEN_WIDTH);
  blurShader.setFloat("height", SCREEN_HEIGHT);
  blurShader.setBool("vert", false);
  blurShader.setBool("_fog", fog);
  blurShader.setInt("_blur", blur);
  blurShader.setVec3("fogColor", fogColor);
  */
    /* 

  glActiveTexture(GL_TEXTURE0+0);
  glBindTexture(GL_TEXTURE_2D, image.texture);
  effectShader.setInt("imageTexture", 0);
  glActiveTexture(GL_TEXTURE0+1);
  glBindTexture(GL_TEXTURE_2D, image.depthTexture);
  effectShader.setInt("depthTexture", 1);
  glBindVertexArray(image.vao);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  //Add the GUI
  renderGUI(world);

  //Swap the Window
  SDL_GL_SwapWindow(gWindow);
  */

}

const bool View::shouldClose() {
    return static_cast<bool>(glfwWindowShouldClose(window));
}

void View::cleanup(){
  //Cleanup Models
  //model.cleanup();

  //Cleanup Shaders
  /* 
  defaultShader.cleanup();
  depthShader.cleanup();
  effectShader.cleanup();

  shadow.cleanup();
  image.cleanup();

  //Shutdown IMGUI
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplSDL2_Shutdown();
  ImGui::DestroyContext();

  //Destroy Context and Window
	SDL_GL_DeleteContext( gContext );
	SDL_DestroyWindow( gWindow );

  //Quit SDL subsystems
  TTF_Quit();
  SDL_Quit();

  */
}



template<typename D>
void View::calcFrameTime(){
  //Current Time!
  auto _new = std::chrono::high_resolution_clock::now();
  frameTime = std::chrono::duration_cast<D>(_new - _old).count();
  _old = std::chrono::high_resolution_clock::now();
}


#endif