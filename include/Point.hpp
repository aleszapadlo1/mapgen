#ifndef POINT_H
#define POINT_H

#include <inttypes.h>

#include <Common.hpp>
#include <Raiblock.hpp>

#include <glm/glm.hpp> // glm::vec3

class Point
{
private:
    Color color;
public:
    elevation_type elevation;
    glm::vec3 localNormal;
    glm::vec3 localCoordinate;
    Position position = Position(0,0);


    Point(elevation_type _elevation = 0, struct Color _color = Color());
    uint8_t getColor(const RGB _color);
    void setColor(const Color _colors);
    void transformElevationToRGB(bool grey_scale = false);

    const elevation_type getElevation(){return elevation;}
};

Point::Point(elevation_type _elevation, struct Color _color) : elevation(_elevation), color(_color) {}

uint8_t Point::getColor(const RGB _channel){
    switch (_channel){
        case red  : return color.r;
        case green: return color.g;
        case blue : return color.b;
        case alpha : return color.a;
    }
}

void Point::setColor(const Color _colors){
    color = _colors;
}

//maps incoming number value to RGB color of the point 
void Point::transformElevationToRGB(bool grey_scale){

    if(grey_scale){
        //elevation on the scale on 0 - 255
        const uint8_t channel = std::floor(elevation * 255);
        color = {channel,channel,channel};
        return;
    }

     //land
    if(elevation >= 0.85){
        color = {50,140,35}; //green
        return;  
    }

    //land
    if(elevation >= 0.75){
        color = {40,130,25}; //green
        return;  
    }

    //land
    if(elevation >= 0.65){
        color = {30,120,15}; //green
        return;
    }
    //land
    if(elevation >= 0.60){
        color = {27,115,18}; //green
        return;
    }
    //land
    if(elevation >= 0.55){
        color = {25,110,15}; //green
        return;

    }
    //shore
    if(elevation >= 0.50){
        color = {225,165,45}; //sand-y
        return;

    }
    //water
    if(elevation>= 0.45){
        color = {45,78,224}; //green
        return;
    }
    
    if(elevation >= 0.4){
        color = {40,70,220}; //green
        return;
    }
    
    if(elevation >= 0.3){
        color = {30,60,210}; //green
        return;
    }
    
    if(elevation >= 0.2){
        color = {20,50,200}; //green
        return;
    }

    color = {10,20,190}; //green
}


#endif