How to build:

create build files:
```
cmake .
```

build the project:
```
make
```

run: 
```
./MapGen
```

First Working prototype:
![alt text](FirstDispersedSedimentation.png "Terrain")


### Dependencies
    Erosion System:
    - gcc (compilation)
    - glm (vectors)
    - libnoise (noise initialization)
    
    Renderer:
    - SDL2 (core, image, ttf)
    - OpenGL3
    - GLEW + GLFW
    - ImGUI (included as binaries in the project, no install required)

    //not yet
    - Boost (System, Filesystem)
