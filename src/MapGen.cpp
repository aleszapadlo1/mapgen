// Your First C++ Program
#include <iostream>

#include <Common.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <Map.hpp>
#include <Terrain.hpp>
#include <View.hpp>

#include <Errors.hpp>
#include <Shader.hpp>
#include <VBO.hpp>
#include <EBO.hpp>
#include <VAO.hpp>
#include <Texture.hpp>
#include <Camera.hpp>

GLfloat lightVertices[] =
        { //     COORDINATES     //
                -0.1f, -0.1f,  0.1f,
                -0.1f, -0.1f, -0.1f,
                0.1f, -0.1f, -0.1f,
                0.1f, -0.1f,  0.1f,
                -0.1f,  0.1f,  0.1f,
                -0.1f,  0.1f, -0.1f,
                0.1f,  0.1f, -0.1f,
                0.1f,  0.1f,  0.1f
        };

GLuint lightIndices[] =
        {
                0, 1, 2,
                0, 2, 3,
                0, 4, 7,
                0, 7, 3,
                3, 7, 6,
                3, 6, 2,
                2, 6, 5,
                2, 5, 1,
                1, 5, 4,
                1, 4, 0,
                4, 5, 6,
                4, 6, 7
        };



void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

Measure measure(true);

//when the string is empty, the measurement will not be made
struct MeasurementsList{
     const std::string terrain;
     const std::string map;
     const std::string glfw;
     const std::string frame;
};

const unsigned int width = 800;

const unsigned int height = 800;


int main() {

    // commenting the line out will disable the measurement
    MeasurementsList measurements = {
            .terrain = "Generate terrain",
            .map = "Fill Map class",
            .glfw = "Initializing glfw and gl",
            .frame = "Until first frame"
    };

    unsigned int MAP_DIMENSIONS = 200;

    //std::cout << "Enter the size of the map: ";

    //std::cin >> MAP_DIMENSIONS;
    measure.start(measurements.frame);

    measure.start(measurements.terrain);
    std::cout << "Generating terrain of size:"<< MAP_DIMENSIONS << "\n";
    Terrain t = Terrain(MAP_DIMENSIONS);
    t.fillTerrainWithNoise();
    //t.createSeaLevel(0.49);
    measure.stop(measurements.terrain);

    //t.applyHydroErosion();

    measure.start(measurements.map);
    Map m = Map(MAP_DIMENSIONS);

    m.transformTerrainIntoMap(t);
    //m.calculatePointNormals();
    m.applyHydroErosion();
    m.calculatePointNormals();
    VBO_type * vertices = m.fillVertexBuffer();
    VBO_index_type * indices = m.fillIndexBuffer();
    measure.stop(measurements.map);
    m.generatePPMimage("test");

    //m.heightmap = true;

    measure.start(measurements.glfw);

   // Initialize GLFW
	glfwInit();

	// Tell GLFW what version of OpenGL we are using 
	// In this case we are using OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Tell GLFW we are using the CORE profile
	// So that means we only have the modern functions
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    // Create a GLFWwindow object of 800 by 800 pixels, naming it "MapGen"
	GLFWwindow* window = glfwCreateWindow(width, height, "MapGen", NULL, NULL);
	// Error check if the window fails to create
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	// Introduce the window into the current context
	glfwMakeContextCurrent(window);

	//Load GLAD so it configures OpenGL
	gladLoadGL();
    glCheckError();

	// Specify the viewport of OpenGL in the Window
	// In this case the viewport goes from x = 0, y = 0, to x = 800, y = 800
    // Set the viewport size
    int _width, _height;
    glfwGetFramebufferSize(window, &_width, &_height);
    glViewport(0, 0, _width, _height);

    // Register the framebuffer size callback function

    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    glCheckError();

    measure.stop(measurements.glfw);



    /*glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
*/

	// Generates Shader object using shaders defualt.vert and default.frag
	Shader shaderProgram("include/graphics/shaders/default.vert", "include/graphics/shaders/default.frag");


	// Generates Vertex Array Object and binds it
	VAO VAO1;
    glCheckError();

    VAO1.bind();
    glCheckError();


	// Generates Vertex Buffer Object and links it to vertices
	VBO VBO1(vertices, m.vertexBufferSize * sizeof(VBO_type));
    glCheckError();
	// Generates Element Buffer Object and links it to indices
	EBO EBO1(indices, m.indexBufferSize * sizeof(VBO_index_type));
    glCheckError();

	// Links VBO to VAO
	//VAO1.linkVBO(VBO1, 0);
    constexpr GLuint stride = 9 * sizeof(VBO_type);
    VAO1.linkAttribute(VBO1,0,3,GL_FLOAT,GL_FALSE,stride,nullptr);
    glCheckError();
    VAO1.linkAttribute(VBO1,1,3,GL_FLOAT,GL_FALSE,stride,(void *) (3*sizeof(VBO_type)));
    glCheckError();
    VAO1.linkAttribute(VBO1,2,3,GL_FLOAT,GL_FALSE,stride,(void *) (6*sizeof(VBO_type)));
    glCheckError();
   // VAO1.linkAttribute(VBO1,2,2,GL_FLOAT,GL_FALSE,stride,(void *) (6*sizeof(GLfloat)));

	// Unbind all to prevent accidentally modifying them
	VAO1.unbind();
    glCheckError();
	VBO1.unbind();
    glCheckError();
	EBO1.unbind();
    glCheckError();

    // Shader for light cube
    Shader lightShader("include/graphics/shaders/light.vert", "include/graphics/shaders/light.frag");
    // Generates Vertex Array Object and binds it
    VAO lightVAO;
    lightVAO.bind();
    // Generates Vertex Buffer Object and links it to vertices
    VBO lightVBO(lightVertices, sizeof(lightVertices));
    // Generates Element Buffer Object and links it to indices
    EBO lightEBO(lightIndices, sizeof(lightIndices));
    // Links VBO attributes such as coordinates and colors to VAO
    lightVAO.linkAttribute(lightVBO, 0, 3, GL_FLOAT,GL_FALSE, 3 * sizeof(float), nullptr);
    // Unbind all to prevent accidentally modifying them
    lightVAO.unbind();
    lightVBO.unbind();
    lightEBO.unbind();


    glm::vec4 lightColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    glm::vec3 lightPos = glm::vec3(0.5f, 1.0f, 0.2f);
    glm::mat4 lightModel = glm::mat4(1.0f);
    lightModel = glm::translate(lightModel, lightPos);

    glm::vec3 mapPos = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::mat4 mapModel = glm::mat4(1.0f);
    mapModel = glm::translate(mapModel, mapPos);


    lightShader.activate();
    glCheckError();
    glUniformMatrix4fv(glGetUniformLocation(lightShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(lightModel));
    glCheckError();
    glUniform4f(glGetUniformLocation(lightShader.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);
    glCheckError();

    shaderProgram.activate();
    glCheckError();
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(mapModel));
    glCheckError();
    glUniform4f(glGetUniformLocation(shaderProgram.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);
    glCheckError();
    glUniform3f(glGetUniformLocation(shaderProgram.ID, "lightPos"), lightPos.x, lightPos.y, lightPos.z);
    glCheckError();


    glm::vec3 position = glm::vec3(0.7f,1.4f,1.6f);
    glm::vec3 orientation = glm::vec3(-0.36f,-0.53f,-0.76f);

    Camera camera(width,height,position,orientation);

    //Texture TEX1("res/textures/pepe.png",0);


    //does the unit:0 represent the GL_TEXTURE0?? I guess not
    //TEX1.texUnit(shaderProgram,"tex0",0);

    float rotation = 0.0f;
    double prevTime = glfwGetTime();

    glEnable(GL_DEPTH_TEST);

    measure.stop(measurements.frame);

    glCheckError();

    // Main while loop
	while (!glfwWindowShouldClose(window))
	{


        // Specify the color of the background
		glClearColor(0.27f, 0.43f, 0.47f, 1.0f);
        glCheckError();
		// Clean the back buffer and assign the new color to it
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glCheckError();
		// Tell OpenGL which Shader Program we want to use
		shaderProgram.activate();
        // Simple timer
        double crntTime = glfwGetTime();
        if (crntTime - prevTime >= 1 / 60)
        {
            rotation += 0.5f;
            prevTime = crntTime;
        }

        /*std::cout << "Camera pos x:" << std::to_string(camera.position.x) <<  " y:" << std::to_string(camera.position.y) <<  " z:" << std::to_string(camera.position.z) << std::endl;
        std::cout << "Camera orientation x:" << std::to_string(camera.orientation.x) <<  " y:" << std::to_string(camera.orientation.y) <<  " z:" << std::to_string(camera.orientation.z) << std::endl;*/

        camera.inputs(window);
        camera.updateCamera(45.0f,0.01f,100.0f);
        // Exports the camera Position to the Fragment Shader for specular lighting
        glUniform3f(glGetUniformLocation(shaderProgram.ID, "camPos"), camera.position.x, camera.position.y, camera.position.z);
        camera.sendCameraToShader(shaderProgram,"cameraTransformation");


        //TEX1.bind();

		// Bind the VAO so OpenGL knows to use it
		VAO1.bind();
        glCheckError();
		// Draw primitives, number of indices, datatype of indices, index of indices
        glDrawElements(GL_TRIANGLES, m.indexBufferSize, GL_UNSIGNED_INT, 0);
        glCheckError();

        // Tells OpenGL which Shader Program we want to use
        lightShader.activate();
        glCheckError();

        // Export the camMatrix to the Vertex Shader of the light cube
        camera.sendCameraToShader(lightShader,"cameraTransformation");

        // Bind the VAO so OpenGL knows to use it
        lightVAO.bind();
        glCheckError();
        // Draw primitives, number of indices, datatype of indices, index of indices
        glDrawElements(GL_TRIANGLES, sizeof(lightIndices) / sizeof(int), GL_UNSIGNED_INT, nullptr);

        // Swap the back buffer with the front buffer
		glfwSwapBuffers(window);
        glCheckError();
		// Take care of all GLFW events
		glfwPollEvents();
        glCheckError();
	}



	// Delete all the objects we've created
	VAO1.terminate();
	VBO1.terminate();
	EBO1.terminate();
    //TEX1.terminate();
	shaderProgram.terminate();

    // Delete window before ending the program
	glfwDestroyWindow(window);
	// Terminate GLFW before ending the program
	glfwTerminate();
	return 0;
}